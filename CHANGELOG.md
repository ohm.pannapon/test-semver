# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.17](https://gitlab.com/ohm.pannapon/test-semver/compare/0.0.16...0.0.17) (2022-09-16)

### [0.0.16](https://gitlab.com/ohm.pannapon/test-semver/compare/0.0.15...0.0.16) (2022-09-16)

### [0.0.15](https://gitlab.com/ohm.pannapon/test-semver/compare/0.0.14...0.0.15) (2022-09-16)

### [0.0.14](https://gitlab.com/ohm.pannapon/test-semver/compare/0.0.13...0.0.14) (2022-09-16)

### [0.0.13](https://gitlab.com/ohm.pannapon/test-semver/compare/0.0.12...0.0.13) (2022-09-16)


### Bug Fixes

* fix ci ([2b7368c](https://gitlab.com/ohm.pannapon/test-semver/commit/2b7368c724e1524040b384677b6deb4e93c272f4))
* fix ci ([01aed9b](https://gitlab.com/ohm.pannapon/test-semver/commit/01aed9b805fee1b010fb1b77b975e353fb766987))
* fix ci ([da971ed](https://gitlab.com/ohm.pannapon/test-semver/commit/da971edbf18ec1e72e90c3674cbf245c8efc74c4))

### [0.0.11](https://gitlab.com/ohm.pannapon/test-semver/compare/0.0.10...0.0.11) (2022-09-16)

### [0.0.10](https://gitlab.com/ohm.pannapon/test-semver/compare/v0.0.9...v0.0.10) (2022-09-16)

### [0.0.9](https://gitlab.com/ohm.pannapon/test-semver/compare/v0.0.8...v0.0.9) (2022-09-15)

### [0.0.8](https://gitlab.com/ohm.pannapon/test-semver/compare/v0.0.7...v0.0.8) (2022-09-15)


### Features

* test commit ([03b5fc8](https://gitlab.com/ohm.pannapon/test-semver/commit/03b5fc814da207e36a66867cca16df68da7bff9b))
* test commit2 ([9bdf981](https://gitlab.com/ohm.pannapon/test-semver/commit/9bdf9818bac4336a690ad70e02624816ee4cadf8))
* test commit3 ([839c3dc](https://gitlab.com/ohm.pannapon/test-semver/commit/839c3dcaefcf7fdbbf37e54b08cc07983f493911))

### [0.0.8](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.7...v0.0.8) (2022-09-15)

### [0.0.7](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.4...v0.0.7) (2022-09-15)

### [0.0.4](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.3...v0.0.4) (2022-09-15)
