module.exports = {
  plugins: [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/gitlab"
  ],
  repositoryUrl: "https://gitlab.com/ohm.pannapon/test-semver",
  branches: ['main'],
}